node {
    stage('Prepare Workspace') {
        checkout scm
        sh 'git clean -fdx'
    }

    stage('Build') {
        sh './gradlew build prepareDocker'
    }
    stash name: 'docker', includes: 'build/docker/'
}


def branch2EnvMap = [test: 'Test', stable: 'Prod'];

def branch = env.BRANCH_NAME?.toLowerCase();
def dockerEnv = branch2EnvMap[branch]

if (!dockerEnv) {
    echo "No environment found for branch: ${branch}"
    return;
}

def createImage = false;
def deploy = true;
stage("Create Image and Deploy to ${dockerEnv}?") {
    try {
        timeout(time: 1, unit: 'HOURS') {
            deploy = input(id: 'deploy', message: 'Create Docker Image?',
                    parameters: [[$class: 'BooleanParameterDefinition', defaultValue: true, description: '', name: "Deploy Image to ${dockerEnv}"]])
            createImage = true;
        }
    } catch (err) { // timeout reached or input false
        //ignore
    }
}

if (createImage) {
    node {
        unstash name: 'docker'

        stage('Create Image') {
            sh './build/docker/push-docker-image.sh'

            archiveArtifacts artifacts: 'build/docker/deploy-test.sh'
            archiveArtifacts artifacts: 'build/docker/deploy-prod.sh'
        }

        if (deploy) {
            stage("Deploy Image to ${dockerEnv}") {
                def lowerCaseDockerEnv = dockerEnv.toLowerCase();
                sh "./build/docker/deploy-${lowerCaseDockerEnv}.sh"
            }
        }
    }
}
